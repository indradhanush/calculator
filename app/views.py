# Third party imports
from django.http.response import JsonResponse
from django.views import View
from django.views.generic import TemplateView

# Local imports
from app.cache import calculator_cache
from app.forms import CalculatorForm


class IndexView(TemplateView):
    """
    The landing page of the app.

    :path: /
    """

    template_name = 'index.html'

    def get(self, request):
        return self.render_to_response({'form': CalculatorForm()})


class CalculatorView(View):
    """
    The API to make requests to calculate expressions and retrieve
    latest expressions.

    :path: /calculator/
    """

    def _add(self, a, b):
        return a + b

    def _subtract(self, a, b):
        return a - b

    def _multiply(self, a, b):
        return a * b

    def _divide(self, a, b):
        return a / b

    def calculate(self, form):
        """
        Takes a validated form and returns the result of the
        expression.

        :param form: Form that contains the user inputs. It is
                     required to run `is_valid` on the `form`
                     instance before passing it to this method.
        :type form: app.forms.CalculatorForm

        :return: Result of the expression
        :rtype: decimal.Decimal
        """
        data = form.cleaned_data

        operators = {
            '+': '_add',
            '-': '_subtract',
            '*': '_multiply',
            '/': '_divide',
        }

        attr = operators[data['operator']]
        func = getattr(self, attr)

        return func(data['left_operand'], data['right_operand'])

    def get(self, request):
        expressions = calculator_cache.get()
        return JsonResponse(status=200, data={'expressions': expressions})

    def post(self, request):
        form = CalculatorForm(request.POST)
        if not form.is_valid():
            return JsonResponse(status=400, data={'errors': form.errors})

        answer = self.calculate(form)
        expressions = calculator_cache.set(form, answer)

        return JsonResponse(status=200, data={'expressions': expressions})
