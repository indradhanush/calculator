# System imports
from unittest.mock import Mock

# Third party imports
from django.core.cache import cache
from django.test import TestCase

# Local imports
from app.cache import calculator_cache


class CalculatorCacheTests(TestCase):
    key = calculator_cache.key

    def setUp(self):
        cache.delete(self.key)

    def test_get_with_empty_cache(self):
        self.assertEqual(calculator_cache.get(), [])

    def test_get_with_nonempty_cache(self):
        expressions = ['1 + 2 = 3']
        cache.set(self.key, expressions)
        self.assertEqual(calculator_cache.get(), expressions)

    def test_set(self):
        self.assertFalse(cache.has_key(self.key))

        form = Mock(cleaned_data={
            'left_operand': 1,
            'right_operand': 2,
            'operator': '+',
        })
        calculator_cache.set(form, 3)

        form = Mock(cleaned_data={
            'left_operand': 2,
            'right_operand': 2,
            'operator': '+',
        })
        calculator_cache.set(form, 4)

        self.assertTrue(cache.has_key(self.key))

        expressions = cache.get(self.key)

        self.assertIsInstance(expressions, list)
        self.assertEqual(expressions, ['2 + 2 = 4', '1 + 2 = 3'])

    def test_set_with_exceeded_limit(self):
        expressions = [
            '1 + 2 = 3',
            '2  + 2 = 4',
            '3  + 2 = 5',
            '4  + 2 = 6',
            '5  + 2 = 7',
            '6  + 2 = 8',
            '7  + 2 = 8',
            '8  + 2 = 10',
            '9  + 2 = 11',
            '10  + 2 = 12',
            '11  + 2 = 13',
        ]
        cache.set(self.key, expressions)

        form = Mock(cleaned_data={
            'left_operand': 12,
            'right_operand': 2,
            'operator': '+',
        })
        calculator_cache.set(form, 14)

        expected = ['12 + 2 = 14'] + expressions[:9]

        self.assertEqual(calculator_cache.get(), expected)
