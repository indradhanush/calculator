# System imports
from decimal import Decimal

# Third party imports
from django import forms


class CalculatorForm(forms.Form):
    OPERATORS = (
        ('+', '+'),
        ('-', '-'),
        ('*', '*'),
        ('/', '/'),
    )

    left_operand = forms.DecimalField()

    right_operand = forms.DecimalField()

    operator = forms.ChoiceField(choices=OPERATORS)

    def clean(self):
        cleaned_data = super().clean()

        if self.errors:
            raise forms.ValidationError({})

        denominator = cleaned_data['right_operand']
        operator = cleaned_data['operator']
        if denominator == Decimal('0') and operator == '/':
            raise forms.ValidationError('divide by zero is not permitted')

        return cleaned_data
