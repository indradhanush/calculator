# Third party imports
from django.core.cache import cache
from django.core.urlresolvers import reverse
from django.test import TestCase


class IndexViewTests(TestCase):
    def test_get(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)


class CalculatorViewTests(TestCase):
    def setUp(self):
        # Clear cache before every test
        cache.delete('expressions')

    @property
    def url(self):
        return reverse('app:calculator')

    def test_get(self):
        response = self.client.get(self.url)

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json(), {'expressions': []})

    def test_post_empty_body(self):
        response = self.client.post(self.url)

        self.assertEqual(response.status_code, 400)

        json_response = response.json()['errors']

        self.assertEqual(json_response['left_operand'], ['This field is required.'])
        self.assertEqual(json_response['right_operand'], ['This field is required.'])
        self.assertEqual(json_response['operator'], ['This field is required.'])

    def test_post_invalid_form(self):
        response = self.client.post(self.url, {
            'left_operand': 'a',
            'right_operand': 'b',
            'operator': 'c',
        })

        self.assertEqual(response.status_code, 400)

        json_response = response.json()['errors']

        self.assertEqual(json_response['left_operand'], ['Enter a number.'])
        self.assertEqual(json_response['right_operand'], ['Enter a number.'])
        self.assertEqual(
            json_response['operator'],
            ['Select a valid choice. c is not one of the available choices.']
        )

    def test_post_addition(self):
        response = self.client.post(self.url, {
            'left_operand': 15,
            'right_operand': 10.5,
            'operator': '+',
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['expressions'], ['15 + 10.5 = 25.5'])

    def test_post_subtraction_positive_answer(self):
        response = self.client.post(self.url, {
            'left_operand': 15,
            'right_operand': 10.5,
            'operator': '-',
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['expressions'], ['15 - 10.5 = 4.5'])

    def test_post_subtraction_negative_answer(self):
        response = self.client.post(self.url, {
            'left_operand': 10.5,
            'right_operand': 15,
            'operator': '-',
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['expressions'], ['10.5 - 15 = -4.5'])

    def test_post_multiplication(self):
        response = self.client.post(self.url, {
            'left_operand': 15,
            'right_operand': 10.5,
            'operator': '*',
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['expressions'], ['15 * 10.5 = 157.5'])

    def test_post_division(self):
        response = self.client.post(self.url, {
            'left_operand': 10.5,
            'right_operand': 15,
            'operator': '/',
        })

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json()['expressions'], ['10.5 / 15 = 0.7'])

    def test_post_division_by_zero(self):
        response = self.client.post(self.url, {
            'left_operand': 10.5,
            'right_operand': 0,
            'operator': '/',
        })

        self.assertEqual(response.status_code, 400)
        self.assertEqual(
            response.json()['errors']['__all__'],
            ['divide by zero is not permitted']
        )
