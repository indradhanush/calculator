# Third party imports
from django.conf.urls import url

# Local imports
from app.views import CalculatorView, IndexView


urlpatterns = [
    url(r'^calculator/', CalculatorView.as_view(), name='calculator'),
    url(r'^', IndexView.as_view(), name='index'),
]
