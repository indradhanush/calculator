# Third party imports
from django.core.cache import cache


class _CalculatorCache():
    """
    Wrapper on top of redis to expose `get` and `set` that make more
    sense in terms of the calculator.
    """
    key = 'expressions'
    limit = 10

    def get(self):
        """
        Returns the list of expressions stored in the cache.

        :return: A list of expressions. The length of this is dictated
                 by the value of `limit`.

        :rtype: list
        """
        return cache.get(self.key) or []

    def set(self, form, answer):
        """
        Stores the latest expression as obtained from the validated
        `form` instance and the `answer` of the calculation.

        This method also ensures that we store only the latest
        expressions as dictated by the value of `limit`.

        :param form: The validated form. It is required to have run
                     `is_valid` on the form before passing it to this
                     function.
        :type form: app.forms.CalculatorForm

        :param answer: The answer as obtained by the inputs contained
                       in form.
        :type answer: decimal.Decimal

        :return: A list of expressions. The length of this is dictated
                 by the value of `limit`.

        :rtype: list
        """

        left_operand = form.cleaned_data['left_operand']
        right_operand = form.cleaned_data['right_operand']
        operator = form.cleaned_data['operator']

        expression = f'{left_operand} {operator} {right_operand} = {answer}'

        current = self.get()
        current.insert(0, expression)

        # Get the latest 10 items
        expressions = current[:self.limit]

        cache.set(self.key, expressions, timeout=None)

        return expressions


calculator_cache = _CalculatorCache()
